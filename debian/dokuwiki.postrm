#!/bin/sh
# Postrm script for DokuWiki by Matti Pöllä <mpo@iki.fi>
# Based on postrm for PHPWiki written by Matthew Palmer.

set -e

# Disable apache2 configuration
disable_apache2_conf()
{
	if [ -e /usr/share/apache2/apache2-maintscript-helper ]
	then
		. /usr/share/apache2/apache2-maintscript-helper
		apache2_invoke disconf dokuwiki
	fi
	rm -f /etc/apache2/conf-available/dokuwiki.conf
}


# Reload apache2
reload_apache2()
{
	# Nothing, since `apache2_invoke disconf` already did what had to be done
	:
}



# Above debhelper's additions that clean the debconf database!

# Actions on remove needing debconf
if [ "$1" = "remove" ] && [ -e /usr/share/debconf/confmodule ]; then
	. /usr/share/debconf/confmodule
	# Disable web servers configuration (it makes no sense to keep webservers
	# configured for a wiki that has been removed)
	db_get dokuwiki/system/configure-webserver
	webservers="$RET"
	db_get dokuwiki/system/restart-webserver
	restart="$RET"
	for webserver in $webservers
	do
		webserver=${webserver%,}
		disable_${webserver}_conf "$@"
		if [ "$restart" = "true" ]
		then
			# Note: configure_apache2 uses functions from
			# /usr/share/apache2/apache2-maintscript-helper, which require an
			# unmodified environment, including maintainer script arguments "$@"
			reload_$webserver "$@"
		fi
	done
fi

# Actions on purge needing debconf
if [ "$1" = "purge" ] && [ -e /usr/share/debconf/confmodule ]; then
	. /usr/share/debconf/confmodule
	db_get dokuwiki/system/purgepages || true
	if [ "$RET" = "true" ]; then
		rm -rf /var/lib/dokuwiki/data/attic/*
		rm -rf /var/lib/dokuwiki/data/media/*
		rm -rf /var/lib/dokuwiki/data/media_attic/*
		rm -rf /var/lib/dokuwiki/data/media_meta/*
		rm -rf /var/lib/dokuwiki/data/meta/*
		rm -rf /var/lib/dokuwiki/data/pages/*
		if [ -e /var/lib/dokuwiki/farm ]
		then
			rm -rf /var/lib/dokuwiki/farm
		fi
	fi
fi

# Actions on remove not needing debconf
if [ "$1" = "remove" ]; then
	rm -rf /var/lib/dokuwiki/data/cache
	rm -rf /var/lib/dokuwiki/data/index
	if [ -e /var/lib/dokuwiki/farm ] && [ "$(stat -c '%h' "/var/lib/dokuwiki/farm")" -gt 2 ]
	then
		for site in /var/lib/dokuwiki/farm/* ; do
			rm -rf -- "$site/data/cache"
			rm -rf -- "$site/data/index"
		done
	fi
fi

# Actions on purge not needing debconf
if [ "$1" = "purge" ]; then
	if command -v ucf > /dev/null; then
		ucf --purge /etc/dokuwiki/apache.conf
		ucf --purge /var/lib/dokuwiki/acl/acl.auth.php
		ucf --purge /var/lib/dokuwiki/acl/users.auth.php
		ucf --purge /etc/dokuwiki/htaccess
		ucf --purge /etc/dokuwiki/local.php
	fi

	if command -v ucfr > /dev/null; then
		ucfr --purge dokuwiki /etc/dokuwiki/apache.conf
		ucfr --purge dokuwiki /var/lib/dokuwiki/acl/acl.auth.php
		ucfr --purge dokuwiki /var/lib/dokuwiki/acl/users.auth.php
		ucfr --purge dokuwiki /etc/dokuwiki/htaccess
		ucfr --purge dokuwiki /etc/dokuwiki/local.php
	fi

	for ext in '' '~' '%' .bak .ucf-new .ucf-old .ucf-dist;  do
		rm -f /etc/dokuwiki/apache.conf$ext
		rm -f /var/lib/dokuwiki/acl/acl.auth.php$ext
		rm -f /var/lib/dokuwiki/acl/users.auth.php$ext
		rm -f /etc/dokuwiki/htaccess$ext
		rm -f /etc/dokuwiki/local.php$ext
	done

	if dpkg-statoverride --list /var/lib/dokuwiki/plugins >/dev/null 2>&1; then
		dpkg-statoverride --remove /var/lib/dokuwiki/plugins
	fi

	if dpkg-statoverride --list /etc/dokuwiki >/dev/null 2>&1; then
		dpkg-statoverride --remove /etc/dokuwiki
	fi

	# Remove sites configuration
	if [ -e /etc/dokuwiki/farm ]
	then
		rm -rf /etc/dokuwiki/farm
	fi

	# Remove plugin configuration
	if [ -e /etc/dokuwiki/tpl ]
	then
		rm -rf /etc/dokuwiki/tpl
	fi
fi

# Remove some configuration files backups, cf. preinst upgrade.
if [ "$1" = "abort-upgrade" ] ; then
	rm /usr/share/dokuwiki/.htaccess.upgrade
fi

#DEBHELPER#

# Stop debconf, or the script would stall forever
# (cf. debconf-devel(7) (search for "daemon") and bug #133029)
if [ "$1" = "purge" ]
then
	if [ -e /usr/share/debconf/confmodule ]
	then
		db_stop
	fi
fi

exit 0
